@echo off
IF exist %~dp0backend\.env ( del %~dp0backend\.env )

echo DB_NAME=tutored > %~dp0backend\.env
echo DB_USER=admin >> %~dp0backend\.env
echo DB_PASS=admin >> %~dp0backend\.env
echo DB_CLIENT=postgres >> %~dp0backend\.env
type %~dp0backend\.env
echo SECRET={ce8ad9e6-e924-4b4d-a5b5-b5c2a6d105d7} >> %~dp0backend\.env
echo RESOURCES=%~dp0backend\resources >> %~dp0backend\.env

echo Dotenv file created.

exit