#!/usr/bin/env bash

BACKDIR=$(dirname `readlink -f $0`)
DOTENVFILE=$BACKDIR/backend/.env

[ -d $DOTENVFILE ] && echo "" > $DOTENVFILE

echo DB_NAME=tutored > $DOTENVFILE
echo DB_USER=admin >> $DOTENVFILE
echo DB_PASS=admin >> $DOTENVFILE
echo DB_CLIENT=postgres >> $DOTENVFILE
cat $DOTENVFILE
echo SECRET={ce8ad9e6-e924-4b4d-a5b5-b5c2a6d105d7} >> $DOTENVFILE
echo RESOURCES=$BACKDIR/backend/resources >> $DOTENVFILE

echo ""
echo Dotenv file created.
