import { NewLectureComponent } from './components/dashboard/new-lecture/new-lecture.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EnrolledInCoursesComponent,
         CreateTeacherComponent,
         CourseDetailsComponent,
         CoursesComponent,
         SignUpComponent,
         LoginComponent,
         CreateCourseComponent,
         HomePageComponent,
         DashboardComponent,
         QuizComponent,
         RequestsComponent} from './components';
import { TeacherComponent } from './components/teacher/teacher.component';
import { UserPrefsComponent } from './components/user-prefs/user-prefs.component';
import { AuthGuardService, CourseDetailsGuard, CreateTeacherGuardService } from './services/guards';


const routes: Routes = [

  {
    path: '',
    component: HomePageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'courses',
    component: CoursesComponent
  },
  {
    path: 'courses/create',
    component: CreateCourseComponent
  },
  {
    path: 'users/enrolled',
    component: EnrolledInCoursesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users/created',
    component: TeacherComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users/created/:id/new-lesson/:index',
    component: NewLectureComponent
  },
  {
    path: 'users/created/:id',
    component: TeacherComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users/teacher',
    component: CreateTeacherComponent,
    canActivate: [CreateTeacherGuardService]
  },
  {
    path: 'users/preferences',
    component: UserPrefsComponent,
    canActivate: [AuthGuardService]

  },
  {
    path: 'courses/:id',
    component: CourseDetailsComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
