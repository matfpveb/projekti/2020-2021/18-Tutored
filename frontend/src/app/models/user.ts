export class User {
    id: number;
    email: string;
    token: string;
}

export class UserExtended extends User {
    first_name: string;
    last_name: string;
    phone: string;
    teacher: {
        id: number;
        qualification: string;
        score: number;
        city: string;        
        picture: string;
    };
};