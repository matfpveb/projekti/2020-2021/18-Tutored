export interface CourseModel{
  id: number;
  teacher_id?: number;
  category?: string;
  level?: string;
  name: string;
  description?: string;
  user_id?: number;
  score: number;
}


