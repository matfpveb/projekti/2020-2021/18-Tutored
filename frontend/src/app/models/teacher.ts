export interface TeacherModel{
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  city: string;
  phone: string;
  user_id: number;
  picture: string;
  qualification: string;
}
