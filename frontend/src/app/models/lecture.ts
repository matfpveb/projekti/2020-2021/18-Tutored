export interface LectureModel{
  id: number;
  course_id: number;
  index: number;
  info?: string;
  materials?: any[];
}
