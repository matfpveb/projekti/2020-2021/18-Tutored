import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserExtended } from 'src/app/models';
import { AlertService, AuthenticationService, UserService } from 'src/app/services';

@Component({
  selector: 'app-user-prefs',
  templateUrl: './user-prefs.component.html',
  styleUrls: ['./user-prefs.component.css']
})
export class UserPrefsComponent implements OnInit {
  updateForm: FormGroup;
  loading = false;
  submitted = false;
  user: UserExtended;
  msg: string = "";
  showMsg: boolean = false;
  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private authService: AuthenticationService,
      private userService: UserService) {
        this.user = this.authService.currentUserValue;
      }
  

  ngOnInit(): void {
    this.updateForm = this.formBuilder.group({
      first_name: [this.user.first_name, Validators.required],
      last_name: [this.user.last_name, Validators.required],
      email: [this.user.email, Validators.required],
      phone: [this.user.phone, Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      qualification: [this.user.teacher ? this.user.teacher.qualification : ""],
      city: [this.user.teacher ? this.user.teacher.city : ""]
    });
    this.showMsg = false;
  }

  get getCtrls() { return this.updateForm.controls; }

  onSubmit() {
    this.submitted = true;

    if(this.updateForm.invalid)
      return;
    /*
      const updatedUser = {
      first_name: this.updateForm.value.first_name,
      last_name: this.getCtrls.last_name,
      email: this.getCtrls.email,
      phone: this.getCtrls.phone,
      password: this.getCtrls.password,
      teacher: {
        qualification: this.user.teacher.qualification,
        city: this.user.teacher.city,
        picture: null
      }
    };
    updatedUser.teacher = this.user.teacher;
    */
    this.userService.update(this.updateForm.value)
      .subscribe(
        (data: UserExtended) => {
          console.log(data);
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.msg = "Uspešno promenjeno";
          this.showMsg = true;
        },
        error => {
            this.loading = false;
            this.msg = JSON.stringify(error.message);
            this.showMsg = true;
        })
  }
}
