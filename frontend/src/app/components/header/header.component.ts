import { Router } from '@angular/router';
import { User, UserExtended } from 'src/app/models/user';
import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/app/services';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {  
  constructor(private authenticationService: AuthenticationService,
              private router: Router) {   
  }

  ngOnInit(): void {
  }

  get localUser() {
    return JSON.parse(localStorage.getItem('currentUser'))
  }  

  goToMyCourses(){          
    this.router.navigateByUrl('users/enrolled');
  }

  goToBecomeTeacher() {
    this.router.navigateByUrl('/users/teacher');
  }

  goToCreatedCourses(){
    this.router.navigateByUrl('/users/created');
  }

  goToPrefs() {
    this.router.navigateByUrl('/users/preferences');
  }

  onLogout() {
    this.authenticationService.logout();
  }

}
