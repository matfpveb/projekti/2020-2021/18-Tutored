import { Router } from '@angular/router';
import { CourseModel} from 'src/app/models/courses';
import { Component, OnInit} from '@angular/core';
import { CourseService } from 'src/app/services';
import { TeacherModel } from 'src/app/models/teacher';
import { PageEvent } from '@angular/material/paginator';

interface CoursesAndTeachers{
  course: CourseModel;
  teacher: TeacherModel;
}

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses: CoursesAndTeachers[];
  route: string;
  searchParams;
  filterCourses = [];
  courseSlice = [];
  startIndex = 0;

  constructor(private courseService: CourseService,
              private router: Router) {
                this.route = router.url;
               }

  ngOnInit(): void {
      this.courseService.getAllCourses()
        .subscribe((courses: CoursesAndTeachers[]) => {
          if(this.courseService.currentUser !== null){
            this.courses =courses.filter(x => {
              return x.teacher.user_id !== this.courseService.currentUser.id;
            });
          }else{
            this.courses = courses;
          }
          this.filterCourses = this.courses;
          this.courseSlice = this.filterCourses.slice(0, 5);
        });




  }

  goToDetails(id: number) {
    this.router.navigate([this.route, id]);
  }

  sendRequest(courseId: number){
    this.courseService.sendRequest(courseId).subscribe(() => {

    });
  }

searchCourses($event: any){
  this.searchParams = $event;
  const nameExists: boolean = this.searchParams.courseName !== '';
  const levelExists: boolean = this.searchParams.selectedLevel !== '';
  const categoryExists: boolean = this.searchParams.selectedCategory !== '';
  this.filterCourses = this.courses.filter(x => {
    if (nameExists && levelExists && categoryExists ){
        return (x.course.name.toLowerCase().includes(this.searchParams.courseName.toLowerCase())&&
           x.course.level.toLowerCase().includes( this.searchParams.selectedLevel.toLowerCase())  &&
           x.course.category.toLowerCase() === this.searchParams.selectedCategory.toLowerCase()
          );
    }else if (nameExists && levelExists){
        return (x.course.name.toLowerCase().includes(this.searchParams.courseName.toLowerCase())  &&
              x.course.level.toLowerCase() === this.searchParams.selectedLevel.toLowerCase()
              );
    }else if (nameExists && categoryExists ){
        return (x.course.name.toLowerCase().includes(this.searchParams.courseName.toLowerCase()) &&
                x.course.level.toLowerCase() === this.searchParams.selectedLevel.toLowerCase()
               );
    }else if (levelExists && categoryExists){
        return (x.course.level.toLowerCase() === this.searchParams.selectedLevel.toLowerCase() &&
                x.course.category.toLowerCase() === this.searchParams.selectedCategory.toLowerCase()
              );
    }else if (nameExists){
      return (x.course.name.toLowerCase().includes(this.searchParams.courseName.toLowerCase()));
    }else if (levelExists){
      return ( x.course.level.toLowerCase() === this.searchParams.selectedLevel.toLowerCase());
    }else if (categoryExists){
      return ( x.course.category.toLowerCase() === this.searchParams.selectedCategory.toLowerCase());
    }else {
      return this.courses;
    }




  });

  this.courseSlice = this.filterCourses.slice(0, 5);
  this.startIndex = 0;
}

onPageChange(event: PageEvent){
  this.startIndex = event.pageIndex * event.pageSize;
  let endIndex = this.startIndex + event.pageSize;
  if (endIndex > this.filterCourses.length){
    endIndex = this.filterCourses.length;

  }

  this.courseSlice = this.filterCourses.slice(this.startIndex, endIndex);

}


}
