import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolledInCoursesComponent } from './enrolled-in-courses.component';

describe('EnrolledInCoursesComponent', () => {
  let component: EnrolledInCoursesComponent;
  let fixture: ComponentFixture<EnrolledInCoursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnrolledInCoursesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolledInCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
