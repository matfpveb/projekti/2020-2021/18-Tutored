import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-enrolled-in-courses',
  templateUrl: './enrolled-in-courses.component.html',
  styleUrls: ['./enrolled-in-courses.component.css']
})
export class EnrolledInCoursesComponent implements OnInit {

  courses: any[] = [];
  selectedCourse: any;
  loaded = false;
  constructor(private userService: UserService,
              private router: Router) {
    this.userService.getEnrolledCourses()
        .subscribe( courses => {
                    if(courses) {
                      courses.forEach(c => {
                        if(c.pending === false) {
                          this.courses.push(c);
                          this.selectedCourse = this.selectedCourse || c;
                          console.log(courses);
                        }
                      })
                      this.loaded = true;
                    }
                    else console.error('Failed to get enrolled courses!');
                  });
  }

  ngOnInit(): void {

  }

  noEnrolled() {
    this.router.navigateByUrl('/courses');
  }

}
