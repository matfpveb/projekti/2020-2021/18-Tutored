import { CourseService } from 'src/app/services/course.service';
import { Component, OnInit } from '@angular/core';
import CategoriesRaw from 'src/assets/categories.json';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';


interface Level {
  value: string;
  viewValue: string;
}

interface Category{
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css']
})
export class CreateCourseComponent implements OnInit {

  constructor(private courseService: CourseService,
              private router: Router
              ) {}

  name = '';
  selectedCategory = '';
  selectedLevel = '';
  description = '';

  levels: Level[] = [
    {value: 'pocetni', viewValue: 'Pocetni'},
    {value: 'srednji', viewValue: 'Srednji'},
    {value: 'napredni', viewValue: 'Napredni'}
  ];

  Categories: Category[] = CategoriesRaw;

  ngOnInit(): void {
  }

  submit(){
    this.courseService.createCourse({
      name: this.name,
      user_id: null,
      level: this.selectedLevel,
      category: this.selectedCategory,
      description: this.description
    })
    .pipe().pipe(first())
    .subscribe(
      data => {
          this.router.navigateByUrl('/courses'); // TODO replace with teacher course page
      },
      error => {

            console.log(error);

      });

   }

}
