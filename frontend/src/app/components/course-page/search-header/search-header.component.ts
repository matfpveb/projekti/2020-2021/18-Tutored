import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import CategoriesRaw from 'src/assets/categories.json';

interface Level {
  value: string;
  viewValue: string;
}

interface Category{
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.css']
})
export class SearchHeaderComponent implements OnInit {

  courseName = '';
  selectedLevel = '';
  selectedCategory = '';

  @Output() findCoursesEvent = new EventEmitter<{courseName: string, selectedLevel: string,
  selectedCategory: string}>();



  levels: Level[] = [
    {value: 'pocetni', viewValue: 'Pocetni'},
    {value: 'srednji', viewValue: 'Srednji'},
    {value: 'napredni', viewValue: 'Napredni'}
  ];
  Categories: Category[] = CategoriesRaw;

  sendData(){
    const searchParams = {
      courseName : this.courseName,
      selectedLevel : this.selectedLevel,
      selectedCategory: this.selectedCategory
    };
    this.findCoursesEvent.emit(searchParams);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
