import { ActivatedRoute, Router } from '@angular/router';
import { CourseModel, LectureModel, TeacherModel } from 'src/app/models';
import { UserService, CourseService } from 'src/app/services';
import { Component, OnInit } from '@angular/core';
import { concatMap } from 'rxjs/operators';


@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  id: number;
  loaded = false;
  course: CourseModel;
  teacher: any;

  sentRequest = false;
  success = false;
  msg = '';

  isAccepted = false;
  pending = false;
  constructor(private userService: UserService,
              private aRoute: ActivatedRoute,
              private router: Router,
              private courseService: CourseService) {

                this.id = +this.aRoute.snapshot.paramMap.get('id');
                this.courseService.getBasicCourse(this.id)
                        .subscribe(data => {
                          this.course = data.course;
                          this.teacher = data.teacher;
                          this.loaded = true;
                          console.log(data);
                        });                


             }


  ngOnInit(): void {
    if(!this.loaded)
      return;
    this.courseService.isAccepted(this.id).subscribe(data => {
      if (!data[0].pending){
        this.router.navigate(['/'], {skipLocationChange: true})
        .then(() => this.router.navigate(['users/enrolled']));
      }else if (data[0].pending){
        this.sentRequest = true;
        this.pending = true;
      }
    });

  }

  sendRequest() {
    this.courseService.sendRequest(this.id)
          .subscribe( res => {
            if (res) {
              console.log(res);
              this.sentRequest = true;
              this.success = true;

            }
            else {
              console.error(res);
              this.sentRequest = false;
              this.success = false;
              this.msg = JSON.stringify(res);
            }
          });
  }

  // returnToCourses(){
  //   this.router.navigate(['/courses']);
  // }

  // exist() {
  //   return this.course != null;
  // }

  // isEmpty(){
  //   if(this.course == null)
  //     return true;
  //   return false;
    /*
    this.mediaExists = false;
    this.documentExists = false;
    if(this.materials == null)
      return true;
    for (const x of this.materials){
      if (x.document_url){
        this.documentExists = true;
      }
    }
    for (const x of this.materials){
      if (x.media_url){
        this.mediaExists = true;
      }
    }
    if (this.mediaExists || this.documentExists){
      return true;
    }else{
      return false;
    }
    */
 // }

}

  // TO DO AFTER FIGURING OUT UPLOADING





