export * from './alert';
export * from './header';
export * from './dashboard';
export * from './course-page';
export * from './home-page';
export * from './login';
export * from './sign-up';
export * from './teacher';