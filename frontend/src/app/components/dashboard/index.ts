export * from './dashboard.component';
export * from './homework/homework.component';
export * from './quiz/quiz.component';
export * from './panel/panel.component';
export * from './upload/upload.component';