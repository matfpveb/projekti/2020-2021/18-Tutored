import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LectureModel, HwModel } from 'src/app/models';
import { CourseService, UserService } from 'src/app/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input() accessedCourses: any[];
  @Input() loaded;
  @Input() selectedCourse: any;
  @Input() isTeacher: boolean;
  courseId: number;
  selectedLecture: LectureModel;
  showCreateQuizForm = false;
  showAddHomeworkForm = false;
  hwAssigned: HwModel[] = [];

  showRequest = false;

  enrolledUserIds: number[] = [];
  recievedHomeworks: any = [];
  constructor(private userService: UserService,
              private courseService: CourseService,
              private router: Router,
              ) {
              }

  ngOnInit(): void {
    this.courseId = this.selectedCourse.id;
    this.showRequest = false;
  }

  getOwnedCourse(id: number) {
    this.selectedCourse = this.accessedCourses.find(c => c.id === id);
    if (this.selectedCourse) {
      this.selectedLecture = null;
      // this.showRequest = false;
    }

  }

  // goToRequestsPage(id: number){
  goToRequestsPage(event: Event){
    // console.log(id);
    // this.router.navigate(['/users/requests', id ]);
    this.showRequest = true;
  }

  goToLecture(id: number) {
    if (!id) {
      console.error(`Material id is undefined!`);
      return;
    }
    this.showRequest = false;
    this.courseService.getLectureMaterials(id)
          .subscribe(
            data => {
              if (data) {
                this.selectedLecture = data;
                // console.log(data);
              }
              else {
                console.error(`Failed to fetch material ${id}`);
              }
            }
            );
    this.userService.getEnrolledUserIds(id).subscribe(data => {
      this.recievedHomeworks = [];
      this.enrolledUserIds = [];

           data.forEach(x => {
             this.enrolledUserIds.push(x.user_id);
             if(x.answer_url !== null){
              this.recievedHomeworks.push({first_name: x.first_name, last_name: x.last_name, answer_url: x.answer_url});
             }
           });
    });

    if(!this.isTeacher) {
      this.courseService.getHwAssigned(id)
      .subscribe( data => {
        this.hwAssigned = data;     
        console.log(data);        
      },
      err => {
        console.error(`Failed to fetch HWs\n${err.message}`)
        this.hwAssigned = [];
      });
    }

  }

  postQuiz(){
    this.showCreateQuizForm = !this.showCreateQuizForm;
    this.showAddHomeworkForm = false;
  }

  addHomework(){
    this.showAddHomeworkForm = !this.showAddHomeworkForm;
    this.showCreateQuizForm = false;
  }

  getFilename(filepath: string) {
    return filepath.split('/')[1];
  }

}










