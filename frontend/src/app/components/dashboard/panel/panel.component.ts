import { Router } from '@angular/router';
import { Component, Input, OnInit} from '@angular/core';
import { LectureModel } from 'src/app/models';
import { CourseService } from 'src/app/services';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  @Input() selectedCourse: any;
  @Input() selectedLecture: LectureModel;
  @Input() isTeacher: boolean;
  @Input() showQuiz: boolean;
      
  moreThanAllowedLessons = false;

  constructor(private router: Router,
              private courseService: CourseService) {
  }

  ngOnInit(): void {    
    
  }

  showQuizTest(){
    this.showQuiz = !this.showQuiz;
  }

  closeQuiz($event: boolean){
    this.showQuiz = false;
  }

  createNewLesson(indexes, courseId: number){
    let newIndex;
    if (indexes[0] === null){
      newIndex = 1;
    }else{
      newIndex = indexes.length + 1;
    }

    if(newIndex <= 15){
      this.router.navigate(['users/created', courseId, 'new-lesson', newIndex]);
    }else{
      this.moreThanAllowedLessons = true;
    }

  }

}
