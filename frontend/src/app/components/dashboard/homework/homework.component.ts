import { UserService } from 'src/app/services';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-homework',
  templateUrl: './homework.component.html',
  styleUrls: ['./homework.component.css']
})
export class HomeworkComponent implements OnInit {

  homeworkAssigment: string;
  @Input() lecture_id;
  @Input() user_ids: number[];
  @Input() toAdd;
  @Input() toExamine;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    if(!this.toAdd){
      // this.userService
    }
  }

  postHomework(){
    const homework = {
      homework_assigment: this.homeworkAssigment,
      lecture_id: this.lecture_id,
      user_ids: this.user_ids

    };

    this.userService.postHomework(homework).subscribe(data => {
      this.homeworkAssigment = '';
    });
  }

}
