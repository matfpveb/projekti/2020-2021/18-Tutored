import { UserService } from 'src/app/services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private userService: UserService) { }

  quizForm: FormGroup;
  @Input() lecture_id: number;


  ngOnInit(): void {
    this.quizForm = this.formBuilder.group({
      question: ['', Validators.required],
      answer1: ['', Validators.required],
      answer2: ['', Validators.required],
      answer3: '',
      answer4: '',
      correctAnswer: ['', Validators.required]
    });
  }

  get getCtrls() { return this.quizForm.controls; }

  onSubmit(){
    if (this.quizForm.invalid){
      return;
    }

    const controls = this.getCtrls;


    const quizMaterial = {
      lecture_id:  this.lecture_id,
      question: controls.question.value,
      answer1: controls.answer1.value,
      answer2: controls.answer2.value,
      answer3: controls.answer3.value,
      answer4: controls.answer4.value,
      correct_answer: controls.correctAnswer.value
    };

    this.userService.createQuiz(quizMaterial).subscribe();

  }

}
