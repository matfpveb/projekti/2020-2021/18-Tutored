import { UserService } from 'src/app/services';
import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-quiz-test',
  templateUrl: './quiz-test.component.html',
  styleUrls: ['./quiz-test.component.css']
})
export class QuizTestComponent implements OnInit{


@Input() lectureId: number;
questionAndAnswers: any;
index = 0;
quizAnswers = [];
moreQuestions = false;
numOfQuestions: number;
correctAnswers: number;
result = false;
notFirst = false;
noQuiz = false;
@Output() closeQuizEmmiter = new EventEmitter<boolean>();


  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getQuizzTest(this.lectureId).subscribe(data => {
      if (data[0] === undefined){
        this.noQuiz = true;
      }
      this.questionAndAnswers = data;
      this.numOfQuestions = this.questionAndAnswers.length;
      if (this.numOfQuestions > 1){
        this.moreQuestions = true;
      }
    });

  }



  goToNextQuestion(){
    if (this.index + 1 < this.numOfQuestions){
      this.index = this.index + 1;
      this.notFirst = true;
      if (this.index + 1 === this.numOfQuestions){
        this.moreQuestions = false;
      }
    }else{
      this.moreQuestions = false;
    }
  }

  goToPreviousQuestion(){
    if (this.index > 0){
      this.index = this.index - 1;
      this.moreQuestions = true;
      if (this.index === 0){
        this.notFirst = false;
      }
    }
  }

  showResults(){
    this.userService.seeResult(this.lectureId, this.quizAnswers).subscribe(data => {
      this.correctAnswers = data;
      this.result = true;
    });
  }

  closeQuiz(){
    this.closeQuizEmmiter.emit(false);
  }

  tryAgain(){
    this.index = 0;
    this.result = false;
    if(this.index + 1 === this.numOfQuestions){
      this.moreQuestions = false;
    }else{
      this.moreQuestions = true;
    }

  }

}
