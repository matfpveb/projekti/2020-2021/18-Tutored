import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-lecture',
  templateUrl: './new-lecture.component.html',
  styleUrls: ['./new-lecture.component.css']
})
export class NewLectureComponent implements OnInit {

  constructor(private userService: UserService,
              private aRoute: ActivatedRoute,
              private router: Router) { }

  index = +this.aRoute.snapshot.paramMap.get('index');
  course_id = +this.aRoute.snapshot.paramMap.get('id');
  info = '';

  ngOnInit(): void {

  }

  addNewLecture(){
    this.userService.addNewLesson(this.course_id, this.index, this.info).subscribe(data => {
      this.router.navigate(['/users/created']);
    });
  }
}
