import { UserService } from 'src/app/services';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  @Input() score: number;
  @Input() courseId: number;
  @Input() isTeacher: boolean;
  hasRated: Boolean; // Null if not rated, false if disliked, true if liked
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.hasRated(this.courseId).subscribe(data => {
      this.hasRated = data[0].has_rated;
    });
  }

  rate(like: boolean){
    let inc = 0;
    if (this.hasRated === true && like){
      this.hasRated = null;
      inc = -1;
      this.score -= 1;
    }else if (this.hasRated === null && !like ){
      this.hasRated = false;
      inc = -1;
      this.score -= 1;
    }else if (this.hasRated === false && !like){
      this.hasRated = null;
      inc = 1;
      this.score += 1;
    }else if (this.hasRated === null && like ){
      this.hasRated = true;
      inc = 1;
      this.score += 1;
    }else if (this.hasRated === true && !like ){
      this.hasRated = false;
      inc = -2;
      this.score -= 2;
    }else if (this.hasRated === false && like){
      this.hasRated = true;
      inc = 2;
      this.score += 2;
    }
    this.userService.rate(this.courseId, inc).subscribe(data => {

    });
    this.userService.ChangeHasRated(this.courseId, this.hasRated).subscribe(data => {

    });


  }







}
