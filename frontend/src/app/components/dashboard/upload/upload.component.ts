import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { HwModel } from 'src/app/models';

import { CourseService } from 'src/app/services';

type ProgressInfo = { fileName: string; label: string };

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  @Input() lecture_id: number;
  @Input() hw: HwModel;
  selectedFiles: FileList;
  selectedFileNames: Array<string> = [];  
  progressInfos: any[];  
  constructor(private courseService: CourseService,
              private router: Router) {
                
              }

  ngOnInit(): void {
  }

  selectFile(event: Event): void {
    this.progressInfos = [];
    this.selectedFiles = (event.target as HTMLInputElement).files;
    this.selectedFileNames = [];
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.selectedFileNames.push( this.selectedFiles.item(i).name );
    }
    this.uploadFiles();
  }

  uploadFiles() {
    this.uploadFile(this.lecture_id)
    .subscribe((events: HttpEvent<FormData>[]) => {
        events.forEach((event: HttpEvent<FormData>, index: number) => {
          let percent = 0;
          let label = '';
          const file: File = this.selectedFiles.item(index);
          const progressInfo: ProgressInfo = this.progressInfos[index];

          switch (event.type) {
            case HttpEventType.Sent:
              percent = 0;
              label = `Otpremanje "${file.name}", ${file.size}.`;
              break;

            case HttpEventType.UploadProgress:
              percent = this.calculateProgressPercentage(event.loaded, event.total);
              label = `"${file.name}"  ${percent}% `;
              break;

            case HttpEventType.Response:
              percent = 100;
              label = `"${file.name}" otpremljen!`;
              break;

            default:
              label = `"${file.name}" surprising upload event: ${event.type}.`;
              break;
          }

          progressInfo.label = label;
        });
      });
  }

  uploadObserver(id: number, file: File): Observable<HttpEvent<FormData>> {
    return this.hw ? this.courseService.uploadHw(this.hw.id, file)
              : this.courseService.uploadFile(id, file);
  }

  uploadFile(id: number): Observable<HttpEvent<FormData>[]> {
    const uploadObservables: Observable<HttpEvent<FormData>>[] = [];
    for (let i = 0; i < this.selectedFiles.length; i++) {
      const file: File = this.selectedFiles.item(i);
      const observable: Observable<HttpEvent<FormData>> = this.uploadObserver(id, file);
      uploadObservables.push(observable);

      const progressInfo: ProgressInfo = { fileName: file.name, label: '' };
      this.progressInfos[i] = progressInfo;
    }
    return forkJoin(uploadObservables);
  }

  private calculateProgressPercentage(dataValue: number, dataTotal: number): number {
    return Math.round((dataValue / dataTotal) * 100);
  }
}
