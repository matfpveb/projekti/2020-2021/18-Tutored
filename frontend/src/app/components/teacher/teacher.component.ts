import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from 'src/app/services'

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  accessedCourses: any[];
  loaded = false;
  selectedCourse: any;
  constructor(private userService: UserService,
              private router: Router) { 
    this.userService.getCreatedCourses()
    .subscribe( courses => {
          if(courses) {
              this.accessedCourses = courses;
              this.selectedCourse = this.selectedCourse || this.accessedCourses[0];
              this.loaded = true;
              console.log(courses);
              
          }
          else console.error('Failed to get enrolled courses!');
        });
}

  ngOnInit(): void {
  }

}
