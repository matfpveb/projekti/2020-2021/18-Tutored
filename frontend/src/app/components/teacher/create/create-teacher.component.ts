import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-create-teacher',
  templateUrl: './create-teacher.component.html',
  styleUrls: ['./create-teacher.component.css']
})
export class CreateTeacherComponent implements OnInit {


  loginForm: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      qualification: ['', Validators.required],
      city: ['', Validators.required]

    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

   get getCtrls() { return this.loginForm.controls; }

    onSubmit(){      
      this.userService.createTeacher(this.getCtrls.qualification.value, this.getCtrls.city.value)
      .pipe(first())
      .subscribe(
          data => {
            let currUser = JSON.parse(localStorage.getItem('currentUser'));
            currUser.teacher = data;
            localStorage.setItem('currentUser', JSON.stringify(currUser));
            this.router.navigate(['']);
          },
          error => {              
                console.log(error);              
          });

    }

}


