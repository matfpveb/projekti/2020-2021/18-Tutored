import {  Router } from '@angular/router';
import { UserService } from 'src/app/services';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  requests: any[];
  @Input() courseId: any;
  constructor(private userService: UserService,
              private router: Router) {                         
              }


  ngOnInit(): void {
    if(this.courseId !== undefined) {
      console.log(this.courseId);
      this.userService.getListOfRequests(this.courseId).subscribe(data => {
        this.requests = data;        
      });                  
    }    
  }

  accept(enrollmentId){
    this.userService.acceptRequests(this.courseId, enrollmentId).subscribe(data => {
      /*
      this.router.navigate(['/'], {skipLocationChange: true})
      .then(() => this.router.navigate(['/users/requests', this.courseId]));
      */
      
      this.requests = this.requests.filter(el => el.id !== enrollmentId);
     
    });


  }

}
