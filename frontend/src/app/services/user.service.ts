﻿
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserExtended } from '../models';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private http: HttpClient) {}

    register(user: User): Observable<User> {
      return this.http.post<User>(`/users/register`, user);
    }

    update(user): Observable<UserExtended> {
      return this.http.patch<UserExtended>('/users/', user);
    }

    createTeacher(qualification, city){
      // Teacher component has login guard
      return this.http.post(`/users/teacher`, {qualification, city});
    }

    getEnrolledCourses(): Observable<any[]>{
      return this.http.get<any[]>('/users/enrolled');
    }

    getCreatedCourses(): Observable<any[]>{
      return this.http.get<any[]>(`/users/created`);
    }

    getListOfRequests(id: number): Observable<any[]>{
      return this.http.get<any[]>(`/users/requests/${id}`);
    }


    acceptRequests(id: number, enrollmentId: number){
      return this.http.post(`/users/requests/${id}`, {enrollmentId});
    }

    createQuiz(quizMaterial){
      return this.http.post(`/users/created/quiz`, quizMaterial);
    }

    getEnrolledUserIds(course_id: number): Observable<any>{
      return this.http.get<any>(`/users/created/homework/` + course_id);
    };

    postHomework(homeWork){
      return this.http.post(`/users/created/homework`, homeWork);
    }

    getQuizzTest(lecture_id: number): Observable<any>{
      return this.http.get(`/users/enrolled/quiz/` + lecture_id);
    }

    seeResult(lecture_id: number, quizAnswers: number[]): Observable<number>{
      return this.http.post<number>(`users/enrolled/quiz/` + lecture_id, quizAnswers);
    }

    hasRated(course_id){
      return this.http.get(`users/enrolled/hasRated/`+ course_id);
    }

    ChangeHasRated(course_id:number, has_rated: Boolean){
      return this.http.patch(`users/enrolled/hasRated`, {course_id,has_rated});
    }

    rate(course_id: number, inc: number){
      return this.http.patch(`users/enrolled/score`, {course_id, inc});
    }

    addNewLesson(course_id: number, index: number, info: string){
      return this.http.post(`users/created/new-lecture`, {course_id, index, info});
    }



}
