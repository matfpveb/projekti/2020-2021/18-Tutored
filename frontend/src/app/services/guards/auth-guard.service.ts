import { AuthenticationService } from '../auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,
              private authService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authService.currentUserValue;
        if (currentUser) {
          // If user is already logged in he is automaticly redirected to courses page
          console.log(route.routeConfig.path);
          if (route.routeConfig.path === '') {
            this.router.navigateByUrl('/courses');
          }
          return true;
        }
          else{
            // If user is not logged in he can read homepage else if he tries to acces any other
            // page that is not homepage or courses he gets redirected to login
            if (route.routeConfig.path === ''){
              return true;
            }
            this.router.navigate(['/login'], {queryParams: { returnUrl: state.url }});
            return false;
        }
  }
}
