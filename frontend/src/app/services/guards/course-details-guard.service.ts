import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { CourseService } from 'src/app/services';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserExtended } from 'src/app/models';

@Injectable({ providedIn: 'root' })
export class CourseDetailsGuard implements CanActivate{
  constructor(private courseService: CourseService,
              private router: Router){}  

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){    
    return this.courseService.getCourse(+route.paramMap.get(`id`))
              .pipe(switchMap((course => {                                
                const localUser: UserExtended = this.courseService.currentUser;
                if(!localUser) {
                  this.router.navigateByUrl('/login');
                  return of(false);
                }
                if( localUser.teacher
                 && course.teacher_id === localUser.teacher.id
                 && route.toString().startsWith('/users/created'))
                {
                  console.log(`Rerouting teacher ${course.teacher_id} to teacher course page`);

                  this.router.navigate([`/users/created/${course.id}`]);                   
                  return of(false);
                } 
                
                return of(true);
                
              })))
  }

}

