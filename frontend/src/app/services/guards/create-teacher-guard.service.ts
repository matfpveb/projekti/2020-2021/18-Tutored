import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CreateTeacherGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        
        if (currentUser && !currentUser.teacher) {
          // If user is already logged in he is automaticly redirected to courses page
          // console.log(route.routeConfig.path);          
          
          return true;
        }
        else if(currentUser.teacher) {
          this.router.navigate(['/courses']);          
          return false;
        }
        else {
          // If user is not logged in he can read homepage else if he tries to acces any other
          // page that is not homepage or courses he gets redirected to login
          if (route.routeConfig.path === ''){
            return true;
          }
          this.router.navigate(['/login'], {queryParams: { returnUrl: state.url }});
          return false;
        }                    
  }
}