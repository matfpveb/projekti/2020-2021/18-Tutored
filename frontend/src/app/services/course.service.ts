import { CourseModel, HwModel, LectureModel } from '../models';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient,
              private authService: AuthenticationService) { }

  get currentUser() {
    return this.authService.currentUserValue;
  }
  get reqHeader() {
    return (new HttpHeaders({ 'Content-Type': 'application/json',
                       Authorization: `Bearer ${this.authService.currentUserValue.token}` }));
 }


  getCourse(id: number) {
    return this.http.get<any>(`/courses/detials/${id}`)
  }

  getBasicCourse(id: number) {
    return this.http.get<any>(`/courses/${id}`)
  }

  getAllCourses(): Observable<Array<any>>{
      return this.http.get< Array<any> >(`/courses`);
    }

  createCourse(newCourse): Observable<CourseModel>{
    if(!this.authService.currentUserValue)
      throw "User not logged in";
    return this.http.post<CourseModel>(`/courses/create`, newCourse);
  }

  getLectureMaterials(id: number) {
    if(!this.authService.currentUserValue)
      throw "User not logged in";
    return this.http.get<LectureModel>(`/courses/material/${id}`)
  }

  sendRequest(courseId){
    if(!this.authService.currentUserValue)
      throw "User not logged in";

    return this.http.post(`/courses`, {course_id: courseId} )
  }

  uploadFile(lectureId: number, file: File): Observable<HttpEvent<FormData>> {

    const formData: FormData = new FormData();
    formData.append("file", file);    

    return this.http.request<FormData>( new HttpRequest<FormData>(
      "POST", `/courses/upload/${lectureId}`, formData, { reportProgress: true }
      ));
  }

  uploadHw(hwId: number, file: File): Observable<HttpEvent<FormData>> {

    const formData: FormData = new FormData();
    formData.append("file", file);    

    return this.http.request<FormData>( new HttpRequest<FormData>(
      "PUT", `/courses/homeworks/${hwId}`, formData, { reportProgress: true }
      ));
  }

  getLectures(course_id: number): Observable<any>{
    return this.http.get<any>(`/courses/` + course_id);
  }

  isAccepted(course_id: number): Observable<any>{
    return this.http.get<any>(`/courses/is-accepted/` + course_id);
  }

  getHwAssigned(hwId: number) : Observable<HwModel[]> {
    return this.http.get<HwModel[]>(`/courses/homeworks/${hwId}`);
  }
}
