import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatRadioModule} from '@angular/material/radio';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';


import { HeaderComponent,
         HomePageComponent,
         LoginComponent,
         SignUpComponent,
         AlertComponent,
         CoursesComponent,
         SearchHeaderComponent,
         CourseDetailsComponent,
         CreateCourseComponent,
         CreateTeacherComponent,
         EnrolledInCoursesComponent,
         QuizComponent,
         HomeworkComponent,
         UploadComponent,
         PanelComponent,
         DashboardComponent,
         TeacherComponent,
         RequestsComponent,  } from './components';
import { QuizTestComponent } from './components/dashboard/quiz-test/quiz-test.component';
import { RatingComponent } from './components/dashboard/rating/rating.component';
import { NewLectureComponent } from './components/dashboard/new-lecture/new-lecture.component';

import { httpInterceptorProviders } from './services/http-interceptors';
import { UserPrefsComponent } from './components/user-prefs/user-prefs.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatPaginatorModule

  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    HomePageComponent,
    LoginComponent,
    SignUpComponent,
    AlertComponent,
    CoursesComponent,
    SearchHeaderComponent,
    CourseDetailsComponent,
    CreateCourseComponent,
    CreateTeacherComponent,
    EnrolledInCoursesComponent,
    DashboardComponent,
    QuizComponent,
    HomeworkComponent,
    UploadComponent,
    PanelComponent,
    TeacherComponent,
    RequestsComponent,
    QuizTestComponent,
    RatingComponent,
    NewLectureComponent,
    UserPrefsComponent
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/'},
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
