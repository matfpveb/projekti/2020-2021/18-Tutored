# Project Tutored :clipboard:

Web platform for teaching and advertising courses. It offers lecturer a simple way to organize classes, create quizzes and homework, and share materials with enrolled students.
Course attendees can give a positive or negative mark to the teacher when the course ends.

client: *Angular 11*  
server: *Express* (NodeJS)  
db: *PostgreSQL*  

| Demo video (old) |  | Database schema |
| - | - | - |
| [![Tutored Demo](https://img.youtube.com/vi/Gpl333xRIYI/0.jpg)](https://www.youtube.com/watch?v=Gpl333xRIYI) | | ![Shema](img/dbschema.jpg) |

## Screenshots
| ![04](img/04.jpg) | ![02](img/02.jpg) |
| - | - |
| ![03](img/03.jpg) | ![01](img/01.jpg) |


## Requirements
- nodejs
- npm
- docker

## Usage

### Run configure script  
```
configure.bat   # for Windows
```
```
./configure.sh   # for Linux
```


### Fetch all dependencies  
```
cd backend && npm ci && cd ..
cd frontend && npm ci && cd..
```


### Starting db and server
```
docker-compose up
npm run migrate:l
npm run seed  # optional
npm start
``` 
### Running client
```
  npm start
```


Webpage will be shown on port **4200**

## Developers

- [Filip Nedeljkovic, 305/2017](https://gitlab.com/efen16)
- [Tamara Mikovic, 82/2017](https://gitlab.com/t.mikovic)
- [Sara Kapetinic, 182/2017](https://gitlab.com/SaraKapetinic_mi17182)
- [Djordje Tanaskovic, 94/2017](https://gitlab.com/djordjetane)
- [Predrag Draganovic, 216/2017](https://gitlab.com/predragdraganovic)
