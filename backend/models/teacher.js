const {Model} = require('objection');

class Teacher extends Model {
    static get tableName(){
        return 'teacher';
    }

    static get relationMappings(){
        const Course = require('./course');
        const User = require('./user')
        return {
            courses:{
                relation: Model.HasManyRelation,
                modelClass: Course,
                join:{
                    from: 'teacher.id',
                    to: 'course.teacher_id'
                }
            },
            user:{
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join:{
                    from: 'teacher.user_id',
                    to: 'user.id'
                }
            }
        };
    }
}




module.exports = Teacher;