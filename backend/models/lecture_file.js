const {Model} = require('objection');

class LectureFile extends Model {
    static get tableName(){
        return 'lecture_file';
    }

    static get jsonSchema() {
        return {
          "type": 'object',          
    
          "properties": {
            "id":           { "type": 'integer' },
            "lecture_id":   { "type": 'integer' },
            "type":         { "type": "string" },
            "name":         { "type": "string" },
            "path":         { "type": "string" }
          }
        }
    }

    static get relationMappings(){
        const Lecture = require('./lecture');
        return {
            course:{
                relation: Model.HasOneRelation,
                modelClass: Lecture,
                join:{
                    from: 'lecture_file.lecture_id',
                    to: 'lecture.id'
                }
            }
        };
    }
}


module.exports = LectureFile;