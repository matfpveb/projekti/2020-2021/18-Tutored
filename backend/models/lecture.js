const {Model} = require('objection');

class Lecture extends Model {
    static get tableName(){
        return 'lecture';
    }

    static get jsonSchema() {
        return {
          "type": 'object',
          "required": ['course_id', 'info', 'index'],
    
          "properties": {
            "id": { "type": 'integer' },
            "course_id": { "type": 'integer' },
            "index": { "type": 'integer', "maximum": 15 },
            "info": { "type": 'string' }
          }
        }
    }

    static get relationMappings(){
        const Course = require('./course');
        return {
            course:{
                relation: Model.HasOneRelation,
                modelClass: Course,
                join:{
                    from: 'lecture.course_id',
                    to: 'course.id'
                }
            }
        };
    }
}


module.exports = Lecture;