const {Model} = require('objection');

class Enrollment extends Model {
    static get tableName(){
        return 'enrollment';
    }
    //Might need to fix, cant test now.
    static get relationMappings(){
        const User = require('./user');
        const Course = require('./course');
        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: User,
                join: {
                    from: 'enrollment.user_id',
                    to: 'user.id'
                }
            },
            courses: {
                relation: Model.HasManyRelation,
                modelClass: Course,
                join: {
                    from: 'enrollment.course_id',
                    to: 'course.id'
                }
            }

        };
    }
}


module.exports = Enrollment;