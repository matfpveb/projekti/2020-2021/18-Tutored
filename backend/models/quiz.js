const {Model} = require('objection');

class Quiz extends Model {
    //For some reason all methods need to be static
    static get tableName(){
        return 'quiz';
    }

    static get idColumn(){
        return 'id';
    }

    //  Used for validation and by doc generators
    static get jsonSchema(){
        return {
            "type": 'object',
            "required": ['question', 'answer1', 'answer2', 'correct_answer', 'lecture_id'],

            "properties": {
                "id": { "type": 'integer' },
                "lecture_id":{"type": 'integer'},                
                "question": { "type": 'string'},
                "answer1": { "type": 'string'},
                "answer2": { "type": 'string' },
                "answer3": { "type": 'string' },
                "answer4": {"type": 'string'},
                "correct_answer": {"type": 'integer'}
            }
        }
    }

}

module.exports = Quiz;