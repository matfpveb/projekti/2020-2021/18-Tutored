const {Model} = require('objection');

class User extends Model {
    //For some reason all methods need to be static
    static get tableName(){
        return 'user';
    }

    static get idColumn(){
        return 'id';
    }

    //  Used for validation and by doc generators
    static get jsonSchema(){
        return {
            "type": 'object',
            "required": ['first_name', 'last_name', 'email', 'password'],

            "properties": {
                "id": { "type": 'integer' },                
                "first_name": { "type": 'string', "minLength": 2, "maxLength": 255 },
                "last_name": { "type": 'string', "minLength": 2, "maxLength": 255 },
                "email": { "type": 'string', "minLength": 3, "maxLength": 320 },
                "password": { "type": 'string', "minLength": 6, "maxLength": 255 },
                "phone": {"type": 'string'}
            }
        }
    }

    //Might need fix, cant test right now.
    static get relationMappings(){
        const Enrollment = require('./enrollment');
        return {
            enrollments: {
                relation: Model.HasManyRelation,
                modelClass: Enrollment,
                join: {
                    from: 'user.id',
                    to: 'enrollment.user_id'
                }
            }
        };
    }

}


module.exports = User;