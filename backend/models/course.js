const {Model} = require('objection');

class Course extends Model {
    static get tableName(){
        return 'course';
    }
    static get jsonSchema() {
        return {
          "type": 'object',
          "required": ['category', 'level','name'],
    
          "properties": {
            "id": { "type": 'integer' },
            "teacher_id": { "type": ['integer', 'null'] },
            "name": { "type": 'string', "minLength": 1, "maxLength": 255 },
            "category": { "type": 'string', "minLength": 1, "maxLength": 255 },
            "level": {"type": 'string'},
            "description": {"type": 'string'},
            "date_added": { "type": 'string' },
            "date_deleted": {"type": 'string'},
            "date_updated": {"type": 'string'}
          }
        }
    }

    static get relationMappings(){
        const Teacher = require('./teacher');
        const Lecture = require('./lecture');
        return {
            teacher:{
                relation: Model.HasOneRelation,
                modelClass: Teacher,
                join:{
                    from: 'course.teacher_id',
                    to: 'teacher.id'
                }
            },
            lecture:{
                relation: Model.HasManyRelation,
                modelClass: Lecture,
                join:{
                    from: 'course.id',
                    to: 'lecture.course_id'
                }

            }
        };
    }
    
//TO DO RELATIONAL MAPPING


}


module.exports = Course;