const {Model} = require('objection');

class Homework extends Model {
    //For some reason all methods need to be static
    static get tableName(){
        return 'homework';
    }

    static get idColumn(){
        return 'id';
    }

    //  Used for validation and by doc generators
    static get jsonSchema(){
        return {
            "type": 'object',
            "required": ['assigment', 'user_id', 'lecture_id'],

            "properties": {
                "id": { "type": 'integer' },
                "lecture_id":{"type": 'integer'},                
                "assigment": { "type": 'string', "minLength":2},
                "answer_url": { "type": 'string'},

            }
        }
    }

}

module.exports = Homework;