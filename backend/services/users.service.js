﻿// @ts-nocheck
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const Teacher = require('../models/teacher');
const Enrollment = require('../models/enrollment.js');
const Course = require('../models/course');
const Lecture = require('../models/lecture');
const Quiz = require('../models/quiz');
const Homework = require('../models/homework');


module.exports = {
    authenticate,
    getAll,
    getUserById,
    create,
    update,
    delete: _delete,
    getTeacherByUserId,
    createTeacher,
    getEnrolledCourses,
    getCreatedCourses,
    getRequests,
    acceptRequest,
    createQuiz,
    getAllUserIds,
    postHomework,
    getQuizTest,
    seeResult,
    hasRated, 
    rate,
    changeHasRated,
    createNewLesson
};

async function authenticate({ email, password }) {    
    console.log(`email try login: ${email}`);
    const user = await User.query().where("email", email).first();   
    
    if (!user || !(await bcrypt.compare(password, `${user.password}`)))
        throw 'email or password is incorrect';    
    // authentication successful
    const token = jwt.sign({ sub: user.id }, process.env.SECRET, { expiresIn: '1d' });
    const teacher = (await getTeacherByUserId(user.id));
    
    console.log(`User ${user.id}, ${user.email} teacher=${teacher!=null} logged`);
    return { ...omitHash(user), token, teacher };
}

async function getAll() {
    return await User.query().select('*');
}


async function create(params) {    
    // validation    
   
   if ( await User.query().where('user.email', params.email ).first() ) {
       throw 'email "' + params.email + '" is already taken';
   }

    // hashing password if provided
   
    try {
        params.password = await bcrypt.hash(params.password, 10);
    }
    catch(err) {
        throw err.message;
    }

    const newUser = await User.query().insert({
        first_name: params.first_name,
        last_name: params.last_name,
        email: params.email,        
        phone: params.phone,
        password: params.password
     });
     
    console.log(newUser);
    newUser.teacher = null;
    return newUser;
}

async function update(id, params) {
    const user = await User.query().findById(id);    
    console.log(params);
    // validate
    const emailChanged = params.email && user.email !== params.email;
    if (emailChanged && await User.query().where('email', params.email )) {
        throw 'email "' + params.email + '" is already taken';
    }

    // hash password if it was entered
    if (params.password) {
        params.password = await bcrypt.hash(params.password, 10);
    }

    // copy params to user and save    
    return { ...omitHash( await User.query()        
        .patchAndFetchById(id, {
            first_name: params.first_name,
            last_name: params.last_name,        
            email: params.email,
            phone: params.phone,
            password: params.password
                })        
            ),                          // if teacher params chagned
            teacher: params.teacher ? {  ...(await Teacher.query()
                            .where('user_id', user.id)
                            .patchAndFetch({
                                qualification: params.teacher.qualification,
                                city:          params.teacher.city,
                            })) } 
                            : {     // else fetch teacher
                                ...(await Teacher.query().where('user_id', id))
                            }
            }    
}

async function _delete(id) {
    const deletedUser = await User.query().deleteById(id);
    console.log(`User: ${deletedUser} has been successfully deleted`);
}


async function getUserById(id) {
    const user = await User.query().findById(id);
    if (!user) 
        throw 'User not found';
    return user;
}

function omitHash(user) {
    const { password, ...userWithoutHash } = user;
    return userWithoutHash;
}

async function getTeacherByUserId(id) {
    return await Teacher.query().where('user_id', id).first()
}

async function createTeacher(params) {    
    const teacher = await getTeacherByUserId(params.user_id);    
    if(teacher)
        throw `User ${teacher.id} is already teacher!`;
    
    const newTeacher = await Teacher.query().insert({
        user_id: params.user_id,
        qualification: params.qualification,
        city: params.city
    });    
    console.log(`New teacher added. User id: ${newTeacher.id}`);
    return newTeacher;
}

async function getEnrolledCourses(id){
    return await Course.query()
    .join('enrollment', 'course.id', '=', 'enrollment.course_id')
    .leftJoin('lecture', 'lecture.course_id', '=', 'course.id')
    .select(['course.*', 'enrollment.pending'
            , Lecture.raw(
                'ARRAY_AGG(lecture.id) as lecture_ids'
              )
           ])
    .where('enrollment.user_id', id)
    .groupBy('course.id', 'enrollment.pending');
}

/**
 * Fetching all courses owned by user with their material pkeys
 * @param id - user's pkey 
 */
async function getCreatedCourses(id){                  
   return await Course.query()
   .join('teacher', 'course.teacher_id', '=', 'teacher.id')
   .leftJoin('lecture', 'lecture.course_id', '=', 'course.id')
   .select([
        'course.*',
        Lecture.raw(
              'ARRAY_AGG(lecture.id) as lecture_ids'            
            )
        ])
   .where('teacher.user_id', id)
   .groupBy('course.id');  
}

async function getRequests(params){
    return await Enrollment.query()
    .join('user', 'user.id', '=', 'enrollment.user_id')
    .select('user.first_name', 'user.last_name', 'user.email', 'enrollment.user_id', 'enrollment.id')
    .where('enrollment.pending', true)
    .where('enrollment.course_id', params.id)    
}

async function acceptRequest(params, body){

    const toUpdate = await Enrollment.query().findById(body.enrollmentId);
    const updatedEnrollment = await toUpdate.$query().updateAndFetch({pending: false}); 

    return updatedEnrollment;
}

async function createQuiz(body){    
    return Quiz.query().insert(body);
} 



async function getAllUserIds(params){
    return await Lecture.query()
    .join('enrollment', 'lecture.course_id', '=', 'enrollment.course_id')
    .join('user', 'user.id', '=', 'enrollment.user_id')
    .leftJoin('homework', 'homework.user_id', '=', 'enrollment.user_id')
    .select('enrollment.user_id', 'user.first_name', 'user.last_name', 'homework.answer_url')
    .where('lecture.id', params.id)
    .where('enrollment.pending', false);
}

async function postHomework(body){
   
    for (user_id of body.user_ids) {       
         await Homework.query().insert({
            assigment: body.homework_assigment,
            lecture_id: body.lecture_id,
            user_id: user_id
        });
    }


}

async function getQuizTest(params){
    return await Quiz.query()
    .select('quiz.question', 'quiz.answer1', 'quiz.answer2', 'quiz.answer3', 'quiz.answer4')
    .where('quiz.lecture_id', '=', params.id)
    .orderBy('quiz.id');
}

async function seeResult(req){
    const correctAnswers = await Quiz.query()
    .select('quiz.correct_answer')
    .where('quiz.lecture_id', '=', req.params.id)
    .orderBy('quiz.id');

    let numCorrect = 0;

    for(i=0; i<req.body.length; i++){
        if(req.body[i] === correctAnswers[i].correct_answer){
            numCorrect++;
        }
    }

    return numCorrect;

}


async function hasRated(req){
    return await Enrollment.query()
    .select('enrollment.has_rated')
    .where('enrollment.user_id', req.user.id)
    .andWhere('enrollment.course_id', req.params.id);
}

async function rate(req){
    
    return await Course.query()
    .increment('score', +req.body.inc)
    .where('id', req.body.course_id);
}

async function changeHasRated(req){
    return  await Enrollment.query()
    .update({
        has_rated: req.body.has_rated
            })
    .where('enrollment.user_id', req.user.id)
    .andWhere('enrollment.course_id', req.body.course_id);
     
}

async function createNewLesson(params){
    
    return await Lecture.query()
    .insert({
        course_id: params.course_id,
        index: params.index,
        info: params.info
    })
}

