// @ts-nocheck
const Course = require('../models/course');
const Teacher = require('../models/teacher');
const Enrollment = require('../models/enrollment');
const Lecture = require('../models/lecture');
const LectureFile = require('../models/lecture_file');
const getTeacherByUserId = require('./users.service').getTeacherByUserId;
const { json } = require('express');
const Homework = require('../models/homework');
const User = require('../models/user');

module.exports = {
    getAll,
    getById,
    getByCategory,
    create,
    enroll,
    getMaterial,
    storeFile,
    getLectures,
    isAccepted,
    getHwAssigned,
    putHwSolution
}

async function getAll(){
    return Array.from(await Teacher.query()
    .joinRelated('[courses,user]')
    .select('user.first_name','user.last_name','user.email', 'user.phone',
            'teacher.city', 'courses.id','courses.name','courses.category',
            'courses.level', 'courses.teacher_id', 'courses.description', 'courses.score',  
            'teacher.picture', 'teacher.qualification', 'teacher.user_id'))
    .map(tc => { return {
                    course: {
                        id: tc.id,                    
                        name: tc.name,
                        category: tc.category,
                        level: tc.level,
                        score : tc.score,
                        description: tc.description
                    },
                    teacher: {
                        id: tc.teacher_id,
                        first_name: tc.first_name,
                        last_name: tc.last_name,
                        email: tc.email,
                        city: tc.city,
                        phone: tc.phone,
                        picture: tc.picture,
                        qualification: tc.qualification,
                        user_id: tc.user_id                    
                    }                
                }
        });        
}

async function getById(id) {
    return Array.from(await Teacher.query()
    .joinRelated('[courses,user]')
    .select('user.first_name','user.last_name','user.email', 'user.phone',
            'teacher.city', 'courses.id','courses.name','courses.category',
            'courses.level', 'courses.teacher_id', 'courses.description', 'courses.score',  
            'teacher.picture', 'teacher.qualification', 'teacher.user_id')
    .where('courses.id', id))
    .map(tc => { return {
                    course: {
                        id: tc.id,                    
                        name: tc.name,
                        category: tc.category,
                        level: tc.level,
                        score : tc.score,
                        description: tc.description
                    },
                    teacher: {
                        id: tc.teacher_id,
                        first_name: tc.first_name,
                        last_name: tc.last_name,
                        email: tc.email,
                        city: tc.city,
                        phone: tc.phone,
                        picture: tc.picture,
                        qualification: tc.qualification,
                        user_id: tc.user_id                    
                    }                
                }
        })[0]; 
}
// async function getById(id) {
//     const course = await Course.query().findById(id);
//     if(!course)
//         throw `No course with id ${id}`
            
//     const teacher = await Teacher.query().findById(course.teacher_id);
//     const user = await User.query().findById(teacher_id.user_id);

//     return {
//         course: course,
//         teacher: {
//             ...(teacher),
//             first_name: user.first_name,
//             first_name: user.first_name,
//         }
//     }
// }



async function getByCategory(category) {
    return (await getAll()).filter((c => {
        c === category;
    }))
}

async function getAllTeacherCourses(teacher_id) {
    return await Course.query().where('teacher_id', teacher_id);
}

async function create(params) {
    const exist = await getTeacherByUserId(params.user_id);
    if(!exist)
    throw 'User is not a teacher';
    
    console.log(JSON.stringify(exist));
         
    const names = (await getAllTeacherCourses(exist.id)).every(p => p.name !== params.name);
    if(!names)
        throw 'Teacher has course with same name'
    
    const newCourse = Course.query().insert({
        name: params.name,
        teacher_id: exist.id,
        category: params.category,
        level: params.level,
        description: params.description
    });
    
    return newCourse;
}

async function enroll(params){
    console.log(params);
    const newEnrollment = await Enrollment.query()
    .insert({
        user_id: params.user_id,
        course_id: params.course_id,
        pending: true
    })
    .onConflict(['user_id', 'course_id'])
    .ignore();
    
    console.log(newEnrollment);
    return newEnrollment;    
}

async function getMaterial(id) {    
    const lecture = await Lecture.query().findById(id);    
    if(!lecture)
        throw `Course ${id} does not exist`;
    
    const data = await LectureFile.query()
                        .where('lecture_id', id);
    return {
        id: lecture.id,
        course_id: lecture.course_id,
        index: lecture.index,
        info: lecture.info,
        materials: data
    };
}

async function storeFile(lectureId, file) {
    return await LectureFile.query()
                .insert({
                    lecture_id: lectureId,
                    type:       file.mimetype,
                    name:       file.originalname,
                    path:       `upload/${file.originalname}`
                })
                .onConflict('name')
                .merge()                
}

async function getLectures(req){
    return Array.from(await Course.query()
    .join('enrollment', 'course.id', '=', 'enrollment.course_id')
    .leftJoin('lecture', 'lecture.course_id', '=', 'course.id')
    .select([
        'course.*',
        'enrollment.pending',
        Lecture.raw(
            'ARRAY_AGG(lecture.id) as lec_ids'            
            )
        ])
    .where('enrollment.user_id', req.user.id)
    .andWhere('enrollment.course_id', req.params.id)
    .groupBy('course.id', 'enrollment.pending'))
    .map(el => {el.mat_ids = el.pending ? null : el.mat_ids; return el})
} 

async function isAccepted(req){
    return await  Enrollment.query()
    .select('enrollment.pending')
    .where('enrollment.user_id', req.user.id)
    .andWhere('enrollment.course_id', req.params.id)
    

}

async function getHwAssigned(user_id, lecture_id) { 
    console.log(`User:${user_id} Lecture: ${lecture_id}`);
    return await Homework.query()
    .where('lecture_id', lecture_id)
    .andWhere('user_id', user_id)
 }

async function putHwSolution(hwId, file) {
    console.log(`Put HW:${hwId} File: ${file.originalname}`);
    return await Homework.query()
                .patchAndFetchById(hwId, {                    
                    answer_url: `upload/${file.originalname}`
                })
}