//use from console npx knex migrate:latest
exports.up = function(knex) {
    return knex.schema.
      createTable('user', (table) => {
        table.increments();
        table.string("first_name").notNullable();
        table.string("last_name").notNullable();
        table.string("email").notNullable().unique();
        table.string("phone").notNullable();
        table.string("password").notNullable();        
        table.timestamps(true,true,true);
    }).createTable('teacher', (table) => {
        table.increments();
        table.string("qualification");
        // table.integer("score").notNullable().defaultTo(0);
        table.string("city");
        table.integer("user_id").notNullable().references('id').inTable('user');
        table.timestamps(true,true,true);
        //To change after we find hosting
        table.string("picture").defaultTo('avatar.jpg');
    }).createTable('course', (table) => {
        table.increments();
        table.integer("teacher_id").notNullable().references('id').inTable('teacher');
        table.string("category").notNullable();
        table.string("level").notNullable();
        table.integer("score").notNullable().defaultTo(0);
        table.string("name").notNullable();
        table.string("description");
        table.timestamps(true,true,true);
    }).createTable("lecture", (table) => {
        table.increments();
        table.integer("course_id").notNullable().references("id").inTable("course");        
        table.integer("index").unsigned().notNullable();
        table.string('info').notNullable();
        table.timestamps(true,true,true);
    }).createTable("lecture_file", (table) => {
        table.increments();
        table.integer("lecture_id").notNullable().references("id").inTable("lecture");
        table.string("type");
        table.string("name").unique();
        table.string("path");
    }).createTable("enrollment", (table) => {
        table.increments();
        table.unique(['user_id', 'course_id']);
        table.integer("user_id").notNullable().references("id").inTable("user");
        table.integer("course_id").notNullable().references("id").inTable("course");   
        table.boolean("has_rated").defaultTo(null);       
        table.boolean("pending").notNullable().defaultTo(false);
        table.timestamps(true,true,true);
    }).createTable("quiz", (table) => {
        table.increments();
        table.integer("lecture_id").references("id").inTable("lecture");
        table.string("question").notNullable();
        table.string("answer1").notNullable();
        table.string("answer2").notNullable();
        table.string("answer3");
        table.string("answer4");
        table.integer("correct_answer").unsigned().notNullable();
    }).createTable("homework", (table)=> {
        table.increments();
        table.integer("lecture_id").references("id").inTable("lecture");
        table.integer("user_id").references("id").inTable("enrollment");
        table.string("assigment").notNullable();
        table.string("answer_url");
    })
  };
  
  exports.down = function(knex) {
    return knex.schema
    .dropTableIfExists("lecture")
    .dropTableIfExists("enrollment")
    .dropTableIfExists('homework')
    .dropTableIfExists('quiz')
    .dropTableIfExists("course")
    .dropTableIfExists("teacher")
    .dropTableIfExists('user')
  };
  