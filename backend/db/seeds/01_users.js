
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('user').del()
    .then( () => {
      // Inserts seed entries
      return knex('user').insert([
        {
         first_name: 'Milos', 
         last_name: 'Obilic', 
         email: 'm@m',
         phone: '06413891389',
         password: '$2a$10$K5/Iutf9ObjxByoA9/KlQuJBWFP6eR8YPstT/9sbBTEW9VMtI/W62'
        
        },
        {
         first_name: 'Marko', 
         last_name: 'Markovic', 
         email: 'markomarkovic123@gmail.com',
         phone: '0641234568',
         password: '$2a$10$6ya8gou3I4jifTHw27/S4ObLLa9SFdHdcChDCY0mXVvaG2uNUoDRq'         
        },
        {
         first_name: 'Srecko', 
         last_name: 'Sreckovic', 
         email: 's.sreckovic@gmail.com',
         phone: '0641234569',
         password: '$2a$10$6ya8gou3I4jifTHw27/S4ObLLa9SFdHdcChDCY0mXVvaG2uNUoDRq'
         },
         {
          first_name: 'djordje',
          last_name: 'cvarkov',
          email: 'b@b',
          phone: '0641234567',
          password: '$2a$10$hkoSdHEsXRa5G3SZkLpuf.iGlVyvZ60DlIbD8.U0/Jbh/Lwmz17gC'
        },
        {
          first_name: 'Joe',
          last_name: 'Rogan',
          email: 'j@r.com',
          phone: '21255544156',
          password: '$2a$10$.03nqmtAhPKCoZFfFQp4/O4GWxVBhqIy/k5rc9OmDerVuy7L4tmpG'
        },
        {
          first_name: 'Ada',
          last_name: 'Lovelace',
          email: 'AdaLoveLace@gmail.com',
          phone: '0659531846',
          password: '$2a$10$hkoSdHEsXRa5G3SZkLpuf.iGlVyvZ60DlIbD8.U0/Jbh/Lwmz17gC'
        }
      ]);
    });
};
