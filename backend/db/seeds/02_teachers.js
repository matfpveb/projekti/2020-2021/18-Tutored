
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('teacher').del()
    .then(function () {
      // Inserts seed entries
      return knex('teacher').insert([
        {          
          user_id: 1,
          city: 'Beograd',
          qualification: 'placeholder'
        },
        {           
          user_id: 2,
          city: 'Novi Sad',
          qualification: 'placeholder'
        },
        {
          user_id: 3,
          city: 'Nis',
          qualification: 'placeholder'
        },
        {
          user_id: 6,
          city: 'Leskovac',
          qualification: 'PhD in computer science'
        }
      ]);
    });
};
