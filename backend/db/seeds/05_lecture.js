
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('lecture').del()
    .then(() => {
      // Inserts seed entries
      return knex('lecture').insert([
        {
         course_id: 4,
         index: 1,
         info: "Intro"
        },
        {
          course_id: 4,
          index: 2, 
          info: "Selectors etc."
        },
        {
          course_id: 4,
          index: 3, 
          info: "	Novine uvedene u HTML5..."
        },
        {
          course_id: 6,
          index: 1,
          info: "Klase i metode"
        }
      ]);
    });
};
