
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('enrollment').del()
    .then(() => {
      // Inserts seed entries
      return knex('enrollment').insert([
        {         
         user_id: 1,
         course_id: 2,         
        },
        {          
          user_id: 1,
          course_id: 3,
          pending: false
        },
        {          
          user_id: 2,
          course_id: 1
        },
        {
          user_id:4,
          course_id: 4
        },
        {
          user_id:4,
          course_id: 6
        },
        {
          user_id:4,
          course_id:7
        }

      ]);
    });
};
