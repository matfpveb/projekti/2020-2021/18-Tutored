
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('quiz').del()
    .then(() => {
      // Inserts seed entries
      return knex('quiz').insert([
        {
          lecture_id: 1,
          question: 'Tag za hiperlink je: ',
          answer1: '<link>',
          answer2: '<l>',
          answer3: '<a>',
          answer4: '<route>',
          correct_answer: 3
        },
        {
          lecture_id: 1,
          question: 'Najveci header je: ',
          answer1: 'h1',
          answer2: 'h6',
          correct_answer: 1
        },
        {
          lecture_id: 1,
          question: 'Html je programerski jezik:',
          answer1: 'da',
          answer2: 'ne',
          correct_answer: 2
        }
      ]);
    });
};
