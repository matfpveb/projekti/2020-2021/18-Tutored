
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('course').del()
    .then(function () {
      // Inserts seed entries
      return knex('course').insert([
        {
          teacher_id: 1, 
          category: 'programiranje',
          level: 'pocetni',
          name: 'Python za Pocetnike',
          description: 'Uvod u osnove programiranja kroz python'
        },
        {
          teacher_id: 2, 
          category: 'matematika',
          level: 'srednji',
          name: 'Geometrija',
          description: 'Geometrija u ravni'
        },
        {
          teacher_id: 3, 
          category: 'programiranje',
          level: 'napredni',
          name: 'Algoritmi i strukture podataka',
          description: 'Objasnjenje bitnih algoritama'
        },
        {
          teacher_id: 1, 
          category: 'programiranje',
          level: 'srednji',
          name: 'Html',
          description: 'Html sintaksa'
        },
        {
          teacher_id: 4,
          category: 'programiranje',
          level: 'pocetni',
          name: 'OOP koncepti',
          description: 'OOP koncepti kroz javu'
        },
        {
          teacher_id: 4,
          category: 'programiranje',
          level: 'srednji',
          name: 'Java Spring',
          description: 'Learn to work in popular java framework'
        },
        {
          teacher_id: 1,
          category: 'matematika',
          level: 'napredni',
          name: 'Blockchain programming',
          description: 'Naucite vise o tome kako se coini prave'
        }
      ]);
    });
};
