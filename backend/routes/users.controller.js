﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../middleware/validate-request');
const authorize = require('../middleware/authorize');
const userService = require('../services/users.service');

// routes
router.post('/authenticate', authenticateSchema, authenticate);
router.post('/register', registerSchema, register);
router.get('/current', authorize(), getCurrent);
router.post('/requests/:id', acceptRequest);
router.get('/requests/:id', getRequests);
router.get('/enrolled', authorize(), getEnrolledCourses);
// router.put('/upload/:materialId', authorize(), uploadFile, putUploadFile);

// teacher routes
router.post('/teacher', authorize(), teacherSchema, createTeacher);
router.get('/created', authorize(), getCreatedCourses);
router.post('/created/quiz', authorize(), quizSchema,  createQuiz);
// posts homework assigment for all enrolled users
router.post('/created/homework', authorize(), postHomework);
// gets all user_ids in enrollment table, their names and homework if its done
router.get('/created/homework/:id', authorize(), getAllUserIds)
// base routes
router.get('/:id', authorize(), getById);
router.patch('/', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);
router.get('/', authorize(), getAll);

// gets question and given answers
router.get('/enrolled/quiz/:id', getQuizTest);
router.post('/enrolled/quiz/:id', seeResult)

// checks if user already rated the course
router.get('/enrolled/hasRated/:id', authorize(), hasRated);
// changes how user rated the course
router.patch('/enrolled/hasRated', authorize(), changeHasRated);
// changes score of the course
router.patch('/enrolled/score', authorize(), rate);
router.post('/created/new-lecture', authorize(), createNewLesson);

module.exports = router;




function authenticateSchema(req, res, next) {
    validateRequest(req, next, Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    }))
}

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => res.json(user))
        .catch(next);
}

function registerSchema(req, res, next) {
    validateRequest(req, next, Joi.object({
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required(),
        phone: Joi.string().required(),
        password: Joi.string().min(6).required()
    }))
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({ message: 'Registration successful' }))
        .catch(next);
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(next);
}

function getCurrent(req, res, next) {
    res.json(req.user);
}

function getById(req, res, next) {
    userService.getUserById(req.params.id)
        .then(user => res.json(user))
        .catch(next);
}

function updateSchema(req, res, next) {
    validateRequest(req, next,  Joi.object({
        first_name: Joi.string().empty(''),
        last_name: Joi.string().empty(''),
        email: Joi.string().empty(''),
        phone: Joi.string().min(6).empty(''),
        password: Joi.string().min(6).empty(''),   
        teacher: Joi.object({            
            qualification: Joi.string(),            
            city: Joi.string().min(2),
            picture: Joi.string()
        })     
    }));
}

function update(req, res, next) {
    userService.update(req.user.id, req.body)
        .then(user => res.json(user))
        .catch(next);
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({ message: 'User deleted successfully' }))
        .catch(next);
}

function teacherSchema(req,res,next) {    
    validateRequest(req, next, Joi.object({
        user_id: Joi.number(),
        qualification: Joi.string().empty(''),
        city: Joi.string().empty('')
    }));
}

function createTeacher(req, res, next) {    
    userService.createTeacher(req.body)
        .then((teacher) => res.json(teacher))
        .catch(next);
}

function getEnrolledCourses(req,res,next){
    userService.getEnrolledCourses(req.user.id)
        .then(courses => res.json(courses))
        .catch(next);
}

function getCreatedCourses(req,res,next){
    userService.getCreatedCourses(req.user.id)
    .then(courses => res.json(courses))
    .catch(next);
}

function getRequests(req, res, next){
    userService.getRequests(req.params)
    .then(requests => res.json(requests))
    .catch(next);
}

function acceptRequest(req,res,next){
    userService.acceptRequest(req.params, req.body)
    .then(request => res.json(request))
    .catch(next);
}

function quizSchema(req,res,next){
    validateRequest(req,next, Joi.object({
        lecture_id: Joi.number().required(),
        question: Joi.string().required(),
        answer1: Joi.string().required(),
        answer2: Joi.string().required(),
        answer3: Joi.string(),
        answer4: Joi.string(),
        correct_answer: Joi.number().required()

    }))
}

function createQuiz(req,res,next){
    userService.createQuiz(req.body)
    .then(quiz => res.json(quiz))
    .catch(next);
}



function getAllUserIds(req, res, next){
    userService.getAllUserIds(req.params)
    .then(data => res.json(data))
    .catch(next);

}

function postHomework(req, res, next){

    userService.postHomework(req.body)
    .then(data => res.json(data))
    .catch(next);

}

function getQuizTest(req, res, next){
    userService.getQuizTest(req.params)
    .then(data => res.json(data))
    .catch(next);
}

function seeResult(req, res, next){
    userService.seeResult(req).
    then(data => res.json(data))
    .catch(next);
}

function hasRated(req, res, next){
    userService.hasRated(req)
    .then(data => res.json(data))
    .catch(next);
}

function rate(req,res,next){
    userService.rate(req)
    .then(data => res.json(data))
    .catch(next);
}

function changeHasRated(req, res, next){
    userService.changeHasRated(req)
    .then(data => res.json())
    .catch(next);
}

function createNewLesson(req, res, next){
    userService.createNewLesson(req.body)
    .then(data => res.json(data))
    .catch(next);
}
