// @ts-nocheck
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../middleware/validate-request');
const authorize = require('../middleware/authorize');
const {uploadFile} = require('../middleware/resourse-handler');
const CourseService = require('../services/courses.service');

router.get('/', getAll);
router.get('/:id', getCourse);
router.get('/is-accepted/:id', authorize(), isAccepted);
router.get('/details/:id', authorize(), getLectures);
router.post('/', authorize(), enroll);
router.post('/create', authorize(), createSchema, create);
router.get('/:category', getByCategory);
router.get('/:category', getByCategory);
router.get('/material/:id', authorize(), getMaterial);
router.post('/upload/:lectureId', authorize(), uploadFile, storeFile);
router.get('/homeworks/:id', authorize(), getHwAssigned);
router.put('/homeworks/:hwId', authorize(), uploadFile, putHwSolution);

module.exports = router;

function getAll(req,res,next) {
    CourseService.getAll()
    .then(courses => res.json(courses))
    .catch(next);    
}

function getCourse(req,res,next) {
    CourseService.getById(req.params.id)
    .then(course => res.json(course))
    .catch(next);    
}

function getLectures(req,res,next){
    CourseService.getLectures(req)
    .then(data => res.json(data))
    .catch(next);

}

function createSchema(req,res,next) {         
    validateRequest(req.body, next, Joi.object({
        name: Joi.string().empty('').required(),
        category: Joi.string().empty('').required(),
        level: Joi.string().empty('').required(),
        description: Joi.string().empty(''),
        user_id: Joi.number().required() // Added by middleware
    }))
}

function create(req,res,next) {    
    CourseService.create(req.body)
    .then(course => res.json(course))
    .catch(next)
}

function getByCategory(category) {
    CourseService.getByCategory(category)
    .then(courses => res.json(courses))
    .catch(next);
}

function enroll(req,res,next){
    CourseService.enroll(req.body)
    .then((enrollment) => res.json(enrollment))
    .catch(next);
}

function getMaterial(req,res,next) {
    CourseService.getMaterial(req.params.id)
    .then(material => res.json(material))
    .catch(next)
}

function storeFile(req,res,next) {
    CourseService.storeFile(+req.params.lectureId, req.file)
    .then(material => res.json(material))
    .catch(next)
}

function isAccepted(req,res,next){
    CourseService.isAccepted(req)
    .then(id => res.json(id))
    .catch(next)
}

function getHwAssigned(req,res,next) {    
    CourseService.getHwAssigned(req.user.id, req.params.id)
    .then(hw => res.json(hw))
    .catch(next)
}

function putHwSolution(req,res,next) {
    CourseService.putHwSolution(+req.params.hwId, req.file)
    .then(hw => res.json(hw))
    .catch(next)
}

module.exports = router;

