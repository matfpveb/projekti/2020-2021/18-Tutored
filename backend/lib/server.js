const http = require('http')
const app = require('./app')
const port = process.env.PORT || 3000;

class Server {
    constructor() {
        this.server = http.createServer(app);
        this.server.listen(port);        
    }

    static run() {
        (new Server).server.once('listening', () => {
            console.log(`Listening on port: ${port}`);
        });
    }
}

module.exports = Server;