const fs = require('fs');
global.RESOURCE_DIR = process.env.RESOURCES || "../resources";
if (!fs.existsSync(RESOURCE_DIR)) {
  fs.mkdirSync(RESOURCE_DIR);
}

const express = require('express');
const app = express();
const cors = require('cors');
const errorHandler = require('../middleware/error-handler')
const compression = require('compression');

app.use(compression());
app.use(express.urlencoded({extended : false}));
app.use(express.json());

/* CORS */
app.use(cors());  

if(process.env.PATH_PUBLIC)
    app.use(express.static(process.env.PATH_PUBLIC));
else
    console.info('Running server for frontend dev mode (start frontend if not)');

app.use(express.static(RESOURCE_DIR));

app.use('/users', require('../routes/users.controller'));
app.use('/courses', require('../routes/courses.controller'));
app.use(errorHandler);

  
module.exports = app;