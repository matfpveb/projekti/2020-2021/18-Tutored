const util = require('util');
const multer = require('multer');
const MaxFileSize = 1024 * 1024 * 1024;


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${RESOURCE_DIR}/upload`);
  },
  filename: (req, file, cb) => {
    const newFilename = file.originalname;
    cb(null, newFilename);
  },
});

const uploadFileMul = multer({
  storage: storage,
  limits: { fileSize: MaxFileSize },
}).single('file');


module.exports.uploadFile = util.promisify(uploadFileMul);