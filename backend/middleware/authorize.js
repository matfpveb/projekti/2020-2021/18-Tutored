const jwt = require('express-jwt');
const User = require('../models/user');

const secret = process.env.SECRET;
module.exports = authorize;

function authorize() {
    return [
        
        // authenticate JWT token and attach decoded token to request as req.user
        jwt({ secret, algorithms: ['HS256'] }),
        
        // attach full user record to request object
        async (req, res, next) => {              
            const user = await User.query().where('id', req.user.sub).first();            
            // check user still exists
            if (!user)
                return res.status(401).json({ message: 'Unauthorized' });

            // authorization successful
            req.user = user;
            if(req.body)
                req.body.user_id = req.user.id;            
            next();
        }
    ];
}